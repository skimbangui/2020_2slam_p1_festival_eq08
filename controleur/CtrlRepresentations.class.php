<?php

/**
 * Contrôleur de gestion des representations
 * @author eq08
 * @version 2020
 */

namespace controleur;

use controleur\GestionErreurs;
use modele\metier\Representation;
use modele\dao\RepresentationDAO;
use modele\dao\GroupeDAO;
use modele\dao\LieuDAO;
use modele\dao\Bdd;
use vue\representations\VueConsultationRepresentation;


class CtrlRepresentations extends ControleurGenerique {
    
    /** controleur= representations & action= defaut
     * Afficher la liste des representations      */
    public function defaut() {
        $this->consulter();
    }

    /** controleur= representations & action= consulter
     * Afficher la liste des representations       */
    function consulter() {
        $laVue = new VueConsultationRepresentation();
        $this->vue = $laVue;
        // La vue a besoin de la liste des represations 
        Bdd::connecter();
        $laVue->setLesRepresentations(RepresentationDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Representation");
        $this->vue->afficher();
    }
        
}

