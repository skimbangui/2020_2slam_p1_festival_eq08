<?php
        use modele\dao\RepresentationDAO;
        use modele\dao\GroupeDAO;
        use modele\dao\LieuDAO;
        use modele\dao\Bdd;
        use controleur\Session;
        
require_once __DIR__ . '/../../includes/autoload.inc.php';

        $id = '1';
        Session::demarrer();
        Bdd::connecter();
 ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>RepresentationDAO : test</title>
    </head>

    <body>

<?php
        echo "<h2>1- RepresentationDAO</h2>";

        // Test n°1
        echo "<h3>Test getOneById</h3>";
        try {
            $objet = RepresentationDAO::getOneById($id);
            var_dump($objet);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        // Test n°2
        echo "<h3>2- getAll</h3>";
        try {
            $lesObjets = RepresentationDAO::getAll();
            var_dump($lesObjets);
        } catch (Exception $ex) {
            echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
        }

        Session::arreter();
        Bdd::deconnecter();
        ?>


    </body>
</html>


