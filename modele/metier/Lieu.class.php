<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele\metier;

/**
 * Description of Lieu
 *
 * @author alory@jolsio.net
 */
class Lieu {
    /**
     * id du lieu
     * @var string
     */
    private $id; 
    /**
     * nom du lieu
     * @var string
     */

    private $nom;
    /**
     * adresse du lieu
     * @var string
     */

    private $adresse;
    /**
     * capacité du lieu
     * @var interger
     */

    private $capacite;
    
    function __construct(string $id, string $nom, string $adresse, int $capacite) {
        $this->id = $id;
        $this->nom = $nom;
        $this->adresse = $adresse;
        $this->capacite = $capacite;
    }
    
    function getId(): string {
        return $this->id;
    }

    function getNom(): string {
        return $this->nom;
    }

    function getAdresse(): string {
        return $this->adresse;
    }

    function getCapacite(): interger {
        return $this->capacite;
    }

    function setId(string $id) {
        $this->id = $id;
    }

    function setNom(string $nom) {
        $this->nom = $nom;
    }

    function setAdresse(string $adresse) {
        $this->adresse = $adresse;
    }

    function setCapacite(interger $capacite) {
        $this->capacite = $capacite;
    }




}
