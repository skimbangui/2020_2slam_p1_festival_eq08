<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele\metier;

/**
 * Description of Representation
 *
 * @author Bastien TRAINEAU
 */
class Representation {
    
    /*identifiant de la representation ("xx")*
     * @var string
     */
    private $id;
    /* nom du lieu
     * @var string
     */
    private $unLieu;
    /* nom du groupe
     * @var leLieu
     */
    private $unGroupe;
    /* heure de debut de la representation
     * @var string
     */
    private $heureDebut;
    /* heure de fin de la representation
     * @var string
     */
    private $heureFin;
    /* date de la representaiton
     * @var leGroupe
     */
    private $date;
    

    function __construct($id, $unLieu, $unGroupe, $heureDebut, $heureFin, $date) {
        $this->id = $id;
        $this->unLieu = $unLieu;
        $this->unGroupe = $unGroupe;
        $this->heureDebut = $heureDebut;
        $this->heureFin = $heureFin;
        $this->date = $date;
    }

    function getId() {
        return $this->id;
    }

    function getUnLieu() {
        return $this->unLieu;
    }

    function getUnGroupe() {
        return $this->unGroupe;
    }

    function getHeureDebut() {
        return $this->heureDebut;
    }

    function getHeureFin() {
        return $this->heureFin;
    }

    function getDate() {
        return $this->date;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setUnLieu($unLieu) {
        $this->unLieu = $unLieu;
    }

    function setUnGroupe($unGroupe) {
        $this->unGroupe = $unGroupe;
    }

    function setHeureDebut($heureDebut) {
        $this->heureDebut = $heureDebut;
    }

    function setHeureFin($heureFin) {
        $this->heureFin = $heureFin;
    }

    function setDate($date) {
        $this->date = $date;
    }


    
}



